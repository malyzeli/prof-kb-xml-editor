"use strict";

//noinspection JSUnresolvedFunction
const webpack = require("webpack");

//noinspection JSUnresolvedFunction,JSUnresolvedVariable
module.exports = {

    entry: {
        app: "./src/main.jsx",
        vendor: [
            "babel-polyfill",
            "classnames",
            "file-saver",
            "i18next",
            "immutability-helper",
            "js2xmlparser",
            "lodash",
            "prop-types",
            "react",
            "react-dom",
            "react-fontawesome",
            "react-i18next",
            "react-syntax-highlighter",
            "sanitize-filename",
            "uuid",
            "xml2js",
        ],
    },

    output: {
        path: __dirname + "/dist/build",
        publicPath: "build/",
        filename: "[name].js",
        chunkFilename: "chunk.[id].js",
    },

    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor",
            filename: "vendor.bundle.js",
        }),
    ],

    module: {
        loaders: [
        ],
    },

    resolve: {
        extensions: [
            ".js",
            ".jsx",
            ".less",
        ],
    },
};
