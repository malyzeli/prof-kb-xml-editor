# XML Web Editor React Example

[Project Task List](./tasks.md)

## Prerequisites

* You need to have [Node.js](https://nodejs.org/) installed on your system, version 6 or newer (get Latest if you don't have specific reason to use LTS).

## Available Commands

All commands should be run in project root directory.

* Run `npm install` to download node module dependencies defined in [package.json](./package.json) - this could take some time, depending on your internet connection.

* Run `npm run dev` to start development server on [http://localhost:8080](http://localhost:8080). Server is configured for hot reloading and changed files are recompiled and updated automatically without the need to refresh your browser. Note that it works only for changes in dynamic part of code - if you change some static variables or declarations, then you will have to refresh your browser.

* Run `npm run lint` to make [ESLint](http://eslint.org/) check for errors in all *.js* and *.jsx* files in [src](./src) directory.

* Run `npm run build` to compile application into [dist](./dist) directory, which you can deploy on any web server afterwards. Build script runs `install` first to ensure all module dependencies are present and then `lint` to prevent building project with errors.

## Project Structure

* Main application entry point is [main.jsx](./src/main.jsx) file in [src](./src) directory.

* Enum definitions are in [enums](./src/enums) directory, where you can easily add new item operations and argument classes.

* Application can switch between EN and CS languages. Dictionaries are defined in [locales](./src/locales) directory.

* Build outputs into [dist](./dist) directory, which you can deploy on any web server afterwards.

* Test XML files are placed in [_test](./_test) directory.

## Important Notes

* For optimal development experience enable [ESLint](http://eslint.org/) support in your IDE, that way you can see errors immediately in-place and do not have to wait for build output. In [IntelliJ](https://www.jetbrains.com/idea/) enable `Settings -> Languages & Frameworks -> JavaScript -> Code Quality Tools -> ESLint` with default settings.

* For correct hot reloading functionality, you need to [change file save strategy](https://webpack.github.io/docs/webpack-dev-server.html#working-with-editors-ides-supporting-safe-write) in your editor. In [IntelliJ](https://www.jetbrains.com/idea/) you have to **turn off safe write** in `Settings -> Appearance and Behaviour -> System Settings -> Synchronization -> Use "safe write"`.

* Use [Google Chrome](https://www.google.com/chrome/) for development, make sure you have **source maps** enabled in dev tools settings, and install both [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi) and [Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd) extensions.

### Author

> Jiri Zelinka ([web](http://malyzeli.cz))
>
> mail: zelinji at gmail.com
