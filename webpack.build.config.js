"use strict";

//noinspection JSUnresolvedFunction
const webpack = require("webpack");
//noinspection JSUnresolvedFunction
const Clean = require("clean-webpack-plugin");
//noinspection JSUnresolvedFunction
const ExtractTextPlugin = require("extract-text-webpack-plugin");

//noinspection JSUnresolvedFunction
const baseConfig = require("./webpack.base.config.js");

//noinspection JSUnresolvedFunction
const buildConfig = {

    plugins: baseConfig.plugins.concat([
        new Clean(baseConfig.output.path),
        new webpack.DefinePlugin({
            "process.env": {
                "NODE_ENV": JSON.stringify("production"),
            },
        }),
        new ExtractTextPlugin({
            filename: "[name].css",
            allChunks: true,
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            output: {comments: false},
            compress: {
                warnings: false,
                drop_console: true,
            },
            minimize: true,
            exclude: [/node_modules/],
        }),
    ]),

    module: {
        loaders: baseConfig.module.loaders.concat([
            {
                test: /\.jsx?$/i,
                loaders: [
                    "babel-loader",
                ],
                exclude: [/node_modules/],
            },
            {
                test: /\.less$/i,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        "css-loader",
                        "less-loader",
                    ],
                }),
            },
        ]),
    },
};

//noinspection JSUnresolvedVariable
module.exports = Object.assign({}, baseConfig, buildConfig);
