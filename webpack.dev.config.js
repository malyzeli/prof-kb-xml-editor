"use strict";

//noinspection JSUnresolvedFunction
const Clean = require("clean-webpack-plugin");

//noinspection JSUnresolvedFunction
const baseConfig = require("./webpack.base.config.js");

//noinspection JSUnresolvedFunction,JSUnresolvedVariable,ES6ModulesDependencies
const devConfig = {

    devServer: {
        historyApiFallback: true,
        contentBase: "./dist",
        port: 8080,
        hot: true,
        inline: true,
    },

    plugins: baseConfig.plugins.concat([
        new Clean(baseConfig.output.path),
    ]),

    devtool: "source-map",

    module: {
        loaders: baseConfig.module.loaders.concat([
            {
                test: /\.jsx?$/i,
                loaders: [
                    "react-hot-loader",
                    "babel-loader",
                ],
                exclude: [/node_modules/],
            },
            {
                test: /\.less$/i,
                loaders: [
                    "style-loader",
                    "css-loader?sourceMap",
                    "less-loader?sourceMap",
                ],
            },
        ]),
    },
};

//noinspection JSUnresolvedVariable
module.exports = Object.assign({}, baseConfig, devConfig);
