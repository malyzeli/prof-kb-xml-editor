import classNames from "classnames";
import PropTypes from "prop-types";
import React from "react";
import {translate} from "react-i18next";
import iterateKeys from "../enums/iterateKeys";
import * as model from "../model/model";
import Button from "./common/Button";
import Option from "./common/Option";
import Expression from "./Expression";

class Iterate extends React.PureComponent {
    //noinspection JSUnusedGlobalSymbols,JSCheckFunctionSignatures
    static propTypes = {
        nodeAddress: PropTypes.arrayOf(PropTypes.number).isRequired,
        iterateKey: PropTypes.string.isRequired,
        children: PropTypes.arrayOf(PropTypes.instanceOf(model.Expression)).isRequired,
        onChangeExpressionType: PropTypes.func.isRequired,
        onAddItem: PropTypes.func.isRequired,
        onAddExpression: PropTypes.func.isRequired,
        onAddIterate: PropTypes.func.isRequired,
        onRemoveExpression: PropTypes.func.isRequired,
        onChangeItemKey: PropTypes.func.isRequired,
        onChangeItemOperation: PropTypes.func.isRequired,
        onChangeItemValue: PropTypes.func.isRequired,
        onRemoveItem: PropTypes.func.isRequired,
        onChangeIterateKey: PropTypes.func.isRequired,
        onRemoveIterate: PropTypes.func.isRequired,
        t: PropTypes.func,
    };

    handleChangeKey = (event) => {
        const {nodeAddress, onChangeIterateKey} = this.props;
        onChangeIterateKey(nodeAddress, event.target.value);
    };

    render() {
        const {
            nodeAddress,
            iterateKey,
            children,
            onChangeExpressionType,
            onAddItem,
            onAddExpression,
            onAddIterate,
            onRemoveExpression,
            onChangeItemKey,
            onChangeItemOperation,
            onChangeItemValue,
            onRemoveItem,
            onChangeIterateKey,
            onRemoveIterate,
            t,
        } = this.props;

        return (
            <li className="iterate">
                <div className="line"/>

                <header>
                    <Button className="remove small"
                            icon="times"
                            title={t("editor.button.remove-iterate")}
                            onClick={() => onRemoveIterate(nodeAddress)}
                    />

                    <label title={t("editor.label.iteration-key")}>
                        <select name="iterateKey"
                                className={classNames({"invalid": !iterateKey})}
                                value={iterateKey}
                                title={t("editor.label.iteration-key")}
                                onChange={this.handleChangeKey}
                        >
                            <option/>
                            {
                                iterateKeys.map((type) => (
                                    <Option key={type}
                                            value={type}
                                    />
                                ))
                            }
                        </select>
                    </label>
                </header>

                <ul className="expression-list">
                    {
                        children.length > 0 && (
                            <Expression nodeAddress={nodeAddress.concat(0)}
                                        type={children[0].type}
                                        children={children[0].children}
                                        onChangeExpressionType={onChangeExpressionType}
                                        onAddItem={onAddItem}
                                        onAddExpression={onAddExpression}
                                        onAddIterate={onAddIterate}
                                        onRemoveExpression={onRemoveExpression}
                                        onChangeItemKey={onChangeItemKey}
                                        onChangeItemOperation={onChangeItemOperation}
                                        onChangeItemValue={onChangeItemValue}
                                        onRemoveItem={onRemoveItem}
                                        onChangeIterateKey={onChangeIterateKey}
                                        onRemoveIterate={onRemoveIterate}
                            />
                        )
                    }
                </ul>
            </li>
        );
    }
}

export default translate()(Iterate);
