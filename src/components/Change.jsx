import classNames from "classnames";
import PropTypes from "prop-types";
import React from "react";
import {translate} from "react-i18next";
import changeInvokes from "../enums/changeInvokes";
import * as Model from "../model/model";
import Argument from "./Argument";
import Button from "./common/Button";
import Option from "./common/Option";

class Change extends React.PureComponent {
    //noinspection JSUnusedGlobalSymbols,JSCheckFunctionSignatures
    static propTypes = {
        invoke: PropTypes.string.isRequired,
        invokeArguments: PropTypes.arrayOf(PropTypes.instanceOf(Model.Argument)).isRequired,
        onChangeInvoke: PropTypes.func.isRequired,
        onAddArgument: PropTypes.func.isRequired,
        onRemove: PropTypes.func.isRequired,
        onArgumentChangeClass: PropTypes.func.isRequired,
        onArgumentChangeValue: PropTypes.func.isRequired,
        onRemoveArgument: PropTypes.func.isRequired,
        t: PropTypes.func,
    };

    handleChangeInvoke = (event) => {
        const {onChangeInvoke} = this.props;
        onChangeInvoke(event.target.value);
    };

    handleArgumentChangeClass = (argIndex, targetClass) => {
        const {onArgumentChangeClass} = this.props;
        onArgumentChangeClass(argIndex, targetClass);
    };

    handleArgumentChangeValue = (argIndex, value) => {
        const {onArgumentChangeValue} = this.props;
        onArgumentChangeValue(argIndex, value);
    };

    handleRemoveArgument = (argIndex) => {
        const {onRemoveArgument} = this.props;
        onRemoveArgument(argIndex);
    };

    render() {
        const {
            invoke,
            invokeArguments,
            onAddArgument,
            onRemove,
            t,
        } = this.props;

        return (
            <li className="change">
                <div className="id"/>

                <header>
                    <Button className="remove small"
                            icon="times"
                            title={t("editor.button.remove-change")}
                            onClick={onRemove}
                    />

                    <label title={t("editor.label.invoke")}>
                        {
                            t("editor.label.invoke")
                        }
                        <select name="invoke"
                                className={classNames({"invalid": !invoke})}
                                value={invoke}
                                title={t("editor.label.invoke")}
                                onChange={this.handleChangeInvoke}
                        >
                            <option/>
                            {
                                changeInvokes.map((type) => (
                                    <Option key={type}
                                            value={type}
                                    />
                                ))
                            }
                        </select>
                    </label>
                </header>

                <ul className="argument-list">
                    {
                        invokeArguments.map((arg, argIndex) => (
                            <Argument key={arg.uuid}
                                      targetClass={arg.targetClass}
                                      value={arg.value}
                                      onChangeClass={(targetClass) => this.handleArgumentChangeClass(argIndex, targetClass)}
                                      onChangeValue={(value) => this.handleArgumentChangeValue(argIndex, value)}
                                      onRemove={() => this.handleRemoveArgument(argIndex)}
                            />
                        ))
                    }
                    <li className="argument">
                        <Button icon="code"
                                label={t("editor.button.add-argument")}
                                onClick={onAddArgument}
                        />
                    </li>
                </ul>

            </li>
        );
    }
}

export default translate()(Change);
