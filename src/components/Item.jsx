import classNames from "classnames";
import PropTypes from "prop-types";
import React from "react";
import {translate} from "react-i18next";
import itemKeys from "../enums/itemKeys";
import ItemOperation from "../model/enum/ItemOperation";
import {ItemOperationValueType} from "../model/enum/ItemOperationDef";
import Button from "./common/Button";
import Option from "./common/Option";

class Item extends React.PureComponent {
    //noinspection JSUnusedGlobalSymbols,JSCheckFunctionSignatures
    static propTypes = {
        nodeAddress: PropTypes.arrayOf(PropTypes.number).isRequired,
        itemKey: PropTypes.string.isRequired,
        operation: PropTypes.oneOf(ItemOperation.map((op) => op.name)).isRequired,
        value: PropTypes.string.isRequired,
        onChangeItemKey: PropTypes.func.isRequired,
        onChangeItemOperation: PropTypes.func.isRequired,
        onChangeItemValue: PropTypes.func.isRequired,
        onRemoveItem: PropTypes.func.isRequired,
        t: PropTypes.func,
    };

    handleChangeKey = (event) => {
        const {nodeAddress, onChangeItemKey} = this.props;
        onChangeItemKey(nodeAddress, event.target.value);
    };

    handleChangeOperation = (event) => {
        const {nodeAddress, onChangeItemOperation} = this.props;
        onChangeItemOperation(nodeAddress, event.target.value);
    };

    handleChangeValue = (event) => {
        const {nodeAddress, onChangeItemValue} = this.props;
        onChangeItemValue(nodeAddress, event.target.value);
    };

    render() {
        const {
            nodeAddress,
            itemKey,
            operation,
            value,
            onRemoveItem,
            t,
        } = this.props;

        return (
            <li className="item">
                <div className="id"/>

                <Button className="remove small"
                        icon="times"
                        title={t("editor.button.remove-item")}
                        onClick={() => onRemoveItem(nodeAddress)}
                />

                <select name="itemKey"
                        className={classNames({"invalid": !itemKey})}
                        value={itemKey}
                        title={t("editor.label.key")}
                        onChange={this.handleChangeKey}
                >
                    <option/>
                    {
                        itemKeys.map((type) => (
                            <Option key={type}
                                    value={type}
                            />
                        ))
                    }
                </select>

                <select name="operation"
                        value={operation}
                        title={t("editor.label.operation")}
                        onChange={this.handleChangeOperation}
                >
                    {
                        ItemOperation.map((op) => (
                            <Option key={op.name}
                                    value={op.name}
                            />
                        ))
                    }
                </select>

                <ItemValueInput operation={operation}
                                value={value}
                                onChange={this.handleChangeValue}
                />
            </li>
        );
    }
}

class ItemValueInputNonLocalized extends React.PureComponent {
    //noinspection JSUnusedGlobalSymbols
    static propTypes = {
        operation: PropTypes.oneOf(ItemOperation.map((op) => op.name)).isRequired,
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
        t: PropTypes.func,
    };

    render() {
        const {operation, value, onChange, t} = this.props;

        //noinspection JSUnresolvedFunction
        const operationDef = ItemOperation.enumValueOf(operation);

        const className = classNames({
            invalid: !operationDef.validate(value),
        });

        const sharedProps = {
            value,
            onChange,
            className,
            title: t("editor.label.value"),
            placeholder: t("editor.label.value"),
        };

        if (operationDef.type === ItemOperationValueType.TEXT) {
            return (
                <input type="text"
                       {...sharedProps}
                />
            );
        }

        if (operationDef.type === ItemOperationValueType.NUMBER) {
            return (
                <input type="number"
                       {...sharedProps}
                />
            );
        }

        if (operationDef.type === ItemOperationValueType.SELECT) {
            return (
                <select name="value"
                        {...sharedProps}
                >
                    <option/>
                    {
                        operationDef.values.map((val) => (
                            <Option key={val}
                                    value={val}
                            />
                        ))
                    }
                </select>
            );
        }
    }
}

const ItemValueInput = translate()(ItemValueInputNonLocalized);

export default translate()(Item);
