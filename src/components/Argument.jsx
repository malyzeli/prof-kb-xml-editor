import classNames from "classnames";
import PropTypes from "prop-types";
import React from "react";
import {translate} from "react-i18next";
import ArgumentClass from "../model/enum/ArgumentClass";
import Button from "./common/Button";
import Option from "./common/Option";

class Argument extends React.PureComponent {
    //noinspection JSUnusedGlobalSymbols
    static propTypes = {
        targetClass: PropTypes.oneOf(ArgumentClass.map((ac) => ac.name)).isRequired,
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]).isRequired,
        onChangeClass: PropTypes.func.isRequired,
        onChangeValue: PropTypes.func.isRequired,
        onRemove: PropTypes.func.isRequired,
        t: PropTypes.func,
    };

    handleChangeTargetClass = (event) => {
        const {onChangeClass} = this.props;
        onChangeClass(event.target.value);
    };

    handleChangeValue = (event) => {
        const {onChangeValue} = this.props;
        onChangeValue(event.target.value);
    };

    render() {
        const {
            targetClass,
            value,
            onRemove,
            t,
        } = this.props;

        //noinspection JSUnresolvedFunction
        const argumentClassDef = ArgumentClass.enumValueOf(targetClass);

        const className = classNames({
            invalid: !argumentClassDef.validate(value),
        });

        return (
            <li className="argument">
                <div className="id"/>

                <Button className="remove small"
                        icon="times"
                        title={t("editor.button.remove-argument")}
                        onClick={onRemove}
                />

                <label title={t("editor.label.class")}>
                    {
                        t("editor.label.class")
                    }
                    <select name="class"
                            value={targetClass.fqdn}
                            onChange={this.handleChangeTargetClass}
                    >
                        {
                            ArgumentClass.map((ac) => (
                                <Option key={ac.name}
                                        value={ac.name}
                                />
                            ))
                        }
                    </select>
                </label>

                <label title={t("editor.label.value")}>
                    {
                        t("editor.label.value")
                    }
                    <input type="text"
                           value={value}
                           className={className}
                           placeholder={t("editor.label.value")}
                           onChange={this.handleChangeValue}
                    />
                </label>
            </li>
        );
    }
}

export default translate()(Argument);
