import PropTypes from "prop-types";
import React from "react";
import {translate} from "react-i18next";
import * as Model from "../model/model";
import Change from "./Change";
import Button from "./common/Button";
import Expression from "./Expression";

class Rule extends React.PureComponent {
    //noinspection JSUnusedGlobalSymbols,JSCheckFunctionSignatures
    static propTypes = {
        active: PropTypes.bool.isRequired,
        description: PropTypes.string.isRequired,
        expression: PropTypes.instanceOf(Model.Expression).isRequired,
        changes: PropTypes.arrayOf(PropTypes.instanceOf(Model.Change)).isRequired,
        onMoveUp: PropTypes.func.isRequired,
        onMoveDown: PropTypes.func.isRequired,
        onChangeDescription: PropTypes.func.isRequired,
        onChangeActive: PropTypes.func.isRequired,
        onAddChange: PropTypes.func.isRequired,
        onRemove: PropTypes.func.isRequired,
        onChangeExpressionType: PropTypes.func.isRequired,
        onAddItem: PropTypes.func.isRequired,
        onAddExpression: PropTypes.func.isRequired,
        onAddIterate: PropTypes.func.isRequired,
        onRemoveExpression: PropTypes.func.isRequired,
        onChangeItemKey: PropTypes.func.isRequired,
        onChangeItemOperation: PropTypes.func.isRequired,
        onChangeItemValue: PropTypes.func.isRequired,
        onRemoveItem: PropTypes.func.isRequired,
        onChangeIterateKey: PropTypes.func.isRequired,
        onRemoveIterate: PropTypes.func.isRequired,
        onChangeChangeInvoke: PropTypes.func.isRequired,
        onChangeAddArgument: PropTypes.func.isRequired,
        onRemoveChange: PropTypes.func.isRequired,
        onArgumentChangeClass: PropTypes.func.isRequired,
        onArgumentChangeValue: PropTypes.func.isRequired,
        onRemoveArgument: PropTypes.func.isRequired,
        t: PropTypes.func,
    };

    handleChangeDescription = (event) => {
        const {onChangeDescription} = this.props;
        onChangeDescription(event.target.value);
    };

    handleChangeChangeInvoke = (changeIndex, invoke) => {
        const {onChangeChangeInvoke} = this.props;
        onChangeChangeInvoke(changeIndex, invoke);
    };

    handleChangeAddArgument = (changeIndex) => {
        const {onChangeAddArgument} = this.props;
        onChangeAddArgument(changeIndex);
    };

    handleRemoveChange = (changeIndex) => {
        const {onRemoveChange} = this.props;
        onRemoveChange(changeIndex);
    };

    handleArgumentChangeClass = (changeIndex, argumentIndex, targetClass) => {
        const {onArgumentChangeClass} = this.props;
        onArgumentChangeClass(changeIndex, argumentIndex, targetClass);
    };

    handleArgumentChangeValue = (changeIndex, argumentIndex, value) => {
        const {onArgumentChangeValue} = this.props;
        onArgumentChangeValue(changeIndex, argumentIndex, value);
    };

    handleRemoveArgument = (changeIndex, argumentIndex) => {
        const {onRemoveArgument} = this.props;
        onRemoveArgument(changeIndex, argumentIndex);
    };

    render() {
        const {
            active,
            description,
            expression,
            changes,
            onMoveUp,
            onMoveDown,
            onChangeActive,
            onAddChange,
            onRemove,
            onChangeExpressionType,
            onAddItem,
            onAddExpression,
            onAddIterate,
            onRemoveExpression,
            onChangeItemKey,
            onChangeItemOperation,
            onChangeItemValue,
            onRemoveItem,
            onChangeIterateKey,
            onRemoveIterate,
            t,
        } = this.props;

        return (
            <li className="rule">
                <div className="id"/>

                <header>
                    <div className="buttons-left">
                        <Button icon="level-up"
                                label={t("editor.button.move-up")}
                                onClick={onMoveUp}
                        />

                        <Button icon="level-down"
                                label={t("editor.button.move-down")}
                                onClick={onMoveDown}
                        />
                    </div>

                    <div>
                        <label title={t("editor.label.description")}>
                            {
                                t("editor.label.description")
                            }
                            <input type="text"
                                   value={description}
                                   placeholder={t("editor.label.description")}
                                   onChange={this.handleChangeDescription}
                            />
                        </label>

                        <label title={t("editor.label.active")}>
                            {
                                t("editor.label.active")
                            }
                            <input type="checkbox"
                                   checked={active}
                                   onChange={onChangeActive}
                            />
                        </label>
                    </div>

                    <div className="buttons-right">
                        <Button className="remove"
                                icon="trash-o"
                                label={t("editor.button.remove")}
                                title={t("editor.button.remove-rule")}
                                onClick={onRemove}
                        />
                    </div>
                </header>

                <h3>
                    {
                        t("editor.label.expression")
                    }
                </h3>

                <ul className="expression-list">
                    <Expression nodeAddress={[0]}
                                type={expression.type}
                                children={expression.children}
                                onChangeExpressionType={onChangeExpressionType}
                                onAddItem={onAddItem}
                                onAddExpression={onAddExpression}
                                onAddIterate={onAddIterate}
                                onRemoveExpression={onRemoveExpression}
                                onChangeItemKey={onChangeItemKey}
                                onChangeItemOperation={onChangeItemOperation}
                                onChangeItemValue={onChangeItemValue}
                                onRemoveItem={onRemoveItem}
                                onChangeIterateKey={onChangeIterateKey}
                                onRemoveIterate={onRemoveIterate}
                    />
                </ul>

                <h3>
                    {
                        t("editor.label.changes")
                    }
                </h3>

                <ul className="change-list">
                    {
                        changes.map((change, changeIndex) => (

                            <Change key={change.uuid}
                                    invoke={change.invoke}
                                    invokeArguments={change.invokeArguments}
                                    onChangeInvoke={(invoke) => this.handleChangeChangeInvoke(changeIndex, invoke)}
                                    onAddArgument={() => this.handleChangeAddArgument(changeIndex)}
                                    onRemove={() => this.handleRemoveChange(changeIndex)}
                                    onArgumentChangeClass={(argIndex, targetClass) => this.handleArgumentChangeClass(
                                        changeIndex,
                                        argIndex,
                                        targetClass,
                                    )}
                                    onArgumentChangeValue={(argIndex, value) => this.handleArgumentChangeValue(
                                        changeIndex,
                                        argIndex,
                                        value,
                                    )}
                                    onRemoveArgument={(argIndex) => this.handleRemoveArgument(changeIndex, argIndex)}
                            />
                        ))
                    }
                    <li className="change">
                        <Button icon="mail-forward"
                                label={t("editor.button.add-change")}
                                onClick={onAddChange}
                        />
                    </li>
                </ul>
            </li>
        );
    }
}

export default translate()(Rule);
