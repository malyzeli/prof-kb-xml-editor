import classNames from "classnames";
import PropTypes from "prop-types";
import React from "react";
import {translate} from "react-i18next";
import ExpressionType from "../model/enum/ExpressionType";
import * as model from "../model/model";
import Button from "./common/Button";
import Option from "./common/Option";
import Item from "./Item";
import Iterate from "./Iterate";

const SELECTABLE_EXPRESSION_TYPES = [ExpressionType.AND, ExpressionType.OR];

class ExpressionNonLocalized extends React.PureComponent {
    //noinspection JSUnusedGlobalSymbols,JSCheckFunctionSignatures
    static propTypes = {
        nodeAddress: PropTypes.arrayOf(PropTypes.number).isRequired,
        type: PropTypes.oneOf([ExpressionType.AND, ExpressionType.OR]).isRequired,
        children: PropTypes.arrayOf(
            PropTypes.oneOfType([
                PropTypes.instanceOf(model.Expression),
                PropTypes.instanceOf(model.Iterate),
                PropTypes.instanceOf(model.Item),
            ]),
        ).isRequired,
        onChangeExpressionType: PropTypes.func.isRequired,
        onAddItem: PropTypes.func.isRequired,
        onAddExpression: PropTypes.func.isRequired,
        onAddIterate: PropTypes.func.isRequired,
        onRemoveExpression: PropTypes.func.isRequired,
        onChangeItemKey: PropTypes.func.isRequired,
        onChangeItemOperation: PropTypes.func.isRequired,
        onChangeItemValue: PropTypes.func.isRequired,
        onRemoveItem: PropTypes.func.isRequired,
        onChangeIterateKey: PropTypes.func.isRequired,
        onRemoveIterate: PropTypes.func.isRequired,
        t: PropTypes.func,
    };

    handleChangeType = (event) => {
        const {nodeAddress, onChangeExpressionType} = this.props;
        onChangeExpressionType(nodeAddress, event.target.value);
    };

    render() {
        const {
            nodeAddress,
            type,
            children,
            onChangeExpressionType,
            onAddItem,
            onAddExpression,
            onAddIterate,
            onRemoveExpression,
            onChangeItemKey,
            onChangeItemOperation,
            onChangeItemValue,
            onRemoveItem,
            onChangeIterateKey,
            onRemoveIterate,
            t,
        } = this.props;

        const className = classNames(
            "expression",
            type.toLowerCase(),
        );

        return (
            <li className={className}>
                <div className="line"/>

                <header>
                    {
                        nodeAddress.length > 1 &&
                        <Button className="remove small"
                                icon="times"
                                title={t("editor.button.remove-expression")}
                                onClick={() => onRemoveExpression(nodeAddress)}
                        />
                    }

                    <select name="type"
                            value={type}
                            title={t("editor.label.expression-type")}
                            onChange={this.handleChangeType}
                    >
                        {
                            SELECTABLE_EXPRESSION_TYPES.map((type) => (
                                <Option key={type}
                                        value={type}
                                />
                            ))
                        }
                    </select>
                </header>

                <ul className="expression-list">
                    {
                        children.map((child, childIndex) => {
                            const childNodeAddress = nodeAddress.concat(childIndex);

                            switch (child.type) {
                                case ExpressionType.AND:
                                case ExpressionType.OR:
                                    return (
                                        <Expression nodeAddress={childNodeAddress}
                                                    key={child.uuid}
                                                    type={child.type}
                                                    children={child.children}
                                                    onChangeExpressionType={onChangeExpressionType}
                                                    onAddItem={onAddItem}
                                                    onAddExpression={onAddExpression}
                                                    onAddIterate={onAddIterate}
                                                    onRemoveExpression={onRemoveExpression}
                                                    onChangeItemKey={onChangeItemKey}
                                                    onChangeItemOperation={onChangeItemOperation}
                                                    onChangeItemValue={onChangeItemValue}
                                                    onRemoveItem={onRemoveItem}
                                                    onChangeIterateKey={onChangeIterateKey}
                                                    onRemoveIterate={onRemoveIterate}
                                        />
                                    );

                                case ExpressionType.ITEM:
                                    return (
                                        <Item nodeAddress={childNodeAddress}
                                              key={child.uuid}
                                              itemKey={child.itemKey}
                                              operation={child.operation}
                                              value={child.value}
                                              onChangeItemKey={onChangeItemKey}
                                              onChangeItemOperation={onChangeItemOperation}
                                              onChangeItemValue={onChangeItemValue}
                                              onRemoveItem={onRemoveItem}
                                        />
                                    );

                                case ExpressionType.ITERATE:
                                    return (
                                        <Iterate nodeAddress={childNodeAddress}
                                                 key={child.uuid}
                                                 iterateKey={child.iterateKey}
                                                 children={child.children}
                                                 onChangeExpressionType={onChangeExpressionType}
                                                 onAddItem={onAddItem}
                                                 onAddExpression={onAddExpression}
                                                 onAddIterate={onAddIterate}
                                                 onRemoveExpression={onRemoveExpression}
                                                 onChangeItemKey={onChangeItemKey}
                                                 onChangeItemOperation={onChangeItemOperation}
                                                 onChangeItemValue={onChangeItemValue}
                                                 onRemoveItem={onRemoveItem}
                                                 onChangeIterateKey={onChangeIterateKey}
                                                 onRemoveIterate={onRemoveIterate}
                                        />
                                    );
                            }
                        })
                    }
                    <li className="item">
                        <Button icon="check-square-o"
                                label={t("editor.button.add-item")}
                                onClick={() => onAddItem(nodeAddress)}
                        />
                        <Button icon="share-alt"
                                label={t("editor.button.add-expression")}
                                onClick={() => onAddExpression(nodeAddress)}
                        />
                        <Button icon="repeat"
                                label={t("editor.button.add-iterate")}
                                onClick={() => onAddIterate(nodeAddress)}
                        />
                    </li>
                </ul>
            </li>
        );
    }
}

const Expression = translate()(ExpressionNonLocalized);

export default Expression;
