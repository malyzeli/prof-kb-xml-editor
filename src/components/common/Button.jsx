import PropTypes from "prop-types";
import React from "react";
import FontAwesome from "react-fontawesome";

export default class Button extends React.PureComponent {
    //noinspection JSUnusedGlobalSymbols
    static propTypes = {
        className: PropTypes.string,
        icon: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired,
        label: PropTypes.string,
        title: PropTypes.string,
    };

    //noinspection JSUnusedGlobalSymbols
    static defaultProps = {
        className: "",
    };

    render() {
        const {className, icon, onClick, label, title} = this.props;
        return (
            <button className={className} title={title || label} onClick={onClick}>
                <FontAwesome name={icon} fixedWidth={true}/>
                {
                    label
                }
            </button>
        );
    }
}
