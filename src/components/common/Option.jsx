import PropTypes from "prop-types";
import React from "react";

export default class Option extends React.PureComponent {
    static propTypes = {
        value: PropTypes.node.isRequired,
    };

    render() {
        const {
            value,
        } = this.props;

        return (
            <option value={value}>
                {
                    value
                }
            </option>
        );
    }
}
