import i18n from "i18next";

import cs from "json-loader!./locales/cs/editor.json";
import en from "json-loader!./locales/en/editor.json";

i18n.init({
    resources: {
        cs: {
            translation: cs,
        },
        en: {
            translation: en,
        },
    },
    fallbackLng: "en",
    //debug: true,
});

export default i18n;
