/**
 * Item keys available in editor.
 */
const itemKeys = [
    "message.isdsDmToHands",
    "message.isdsDmSender",
    "message.isdsDmAnnotation",
    "message.isdsDmLegalTitleLaw",
    "message.isdsDmRecipientOrgUnit",
    "message.isdsDmSenderIdent",
    "message.isdsDmSenderOrgUnitNum",
    "message.isdsDmLegalTitleSect",
    "message.isdsDmRecipientRefNumber",
    "message.isdsDbIdSender",
    "message.isdsDmRecipientOrgUnitNum",
    "message.isdsDmSenderOrgUnit",
    "message.isdsDmLegalTitleYear",
    "message.isdsDmPersonalDelivery",
    "message.isdsDmAllowSubstDelivery",
    "message.isdsDmLegalTitlePoint",
    "message.isdsDmLegalTitlePar",
    "message.isdsDmSenderRefNumber",
    "message.isdsDmSenderType",
    "message.isdsDmRecipient",
    "message.isdsDmRecipientIdent",
    "message.isdsDmOvm",
];

export default itemKeys;
