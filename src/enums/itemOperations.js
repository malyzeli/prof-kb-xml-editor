import {ItemOperation, ItemOperationValueType} from "../model/enum/ItemOperationDef";

/**
 * Item operations available in editor. See ItemOperation documentation for params description.
 */
const itemOperations = [

    new ItemOperation(
        "equals",

        ItemOperationValueType.TEXT,
        {
            /** Value must not be empty. */
            validate: (value) => !!value,
        },
    ),

    new ItemOperation(
        "regex",

        ItemOperationValueType.TEXT,
        {
            /** Value must be valid regular expression. */
            validate: (value) => {
                try {
                    new RegExp(value);
                    return true;
                } catch (e) {
                    return false;
                }
            },
        },
    ),

    new ItemOperation(
        "isSenderType",

        ItemOperationValueType.SELECT,
        {
            values: [
                "EXEKUTOR",
                "KATASTRALNI_URAD",
                "REGISTR_SMLUV",
            ],
        },
    ),
];

export default itemOperations;
