/**
 * Invoke methods for change available in editor.
 */
const changeInvokes = [
    "changeBox",
    "changeBoxPCR",
    "changeTemplate",
    "setAttribute",
    "putInQueue",
];

export default changeInvokes;
