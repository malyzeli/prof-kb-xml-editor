import _ from "lodash";
import {ArgumentClass} from "../model/enum/ArgumentClassDef";

/**
 * Argument class available in editor. Maps to default java primitives. See ArgumentClass documentation for params description.
 */
const argumentClasses = [

    new ArgumentClass(
        "Integer",

        "java.lang.Integer",
        {
            validate: (value) => _.isInteger(Number(value)),
        },
    ),

    new ArgumentClass(
        "String",

        "java.lang.String",
        {
            /* any primitive value is accepted, objects and arrays are revoked */
            validate: (value) => !_.isObject(value),
        },
    ),
];

export default argumentClasses;
