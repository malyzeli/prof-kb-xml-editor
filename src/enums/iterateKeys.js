/**
 * Iterate key available in editor.
 */
const iterateKeys = [
    "message.attachments",
];

export default iterateKeys;
