import xml2js from "xml2js";
import ArgumentClass from "../model/enum/ArgumentClass";
import ExpressionType from "../model/enum/ExpressionType";
import {Argument, Change, Expression, Item, Iterate, Rule} from "../model/model";

/**
 * @param string {string}
 * @return {boolean|undefined}
 */
const stringToBoolean = (string) => {
    const arg = string.toLowerCase();
    if (arg === "true") {
        return true;
    }
    if (arg === "false") {
        return false;
    }
    return undefined;
};

/**
 * @param xmlObject {Array}
 * @return {Argument}
 */
function xmlToArgument(xmlObject) {
    //console.log("xmlToArgument", xmlObject);
    const attributes = xmlObject["$"];
    //noinspection JSUnresolvedFunction
    return new Argument(
        ArgumentClass.enumValueOfFqdn(attributes["class"]).name,
        attributes["value"],
    );
}

/**
 * @param xmlObject {object}
 * @return {Change}
 */
function xmlToChange(xmlObject) {
    //console.log("xmlToChange", xmlObject);
    const attributes = xmlObject["$"];
    return new Change(
        attributes["invoke"],
        xmlObject["argument"]
            ? xmlObject["argument"].map(xmlToArgument)
            : [],
    );
}

/**
 * @param xmlObject {object}
 * @return {Change[]}
 */
function xmlToChanges(xmlObject) {
    //console.log("xmlToChanges", xmlObject);
    return xmlObject["change"].map(xmlToChange);
}

/**
 * @param xmlObject {object}
 * @return {Item}
 */
function xmlToItem(xmlObject) {
    //console.log("xmlToItem", xmlObject);
    const attributes = xmlObject["$"];
    return new Item(
        attributes["key"],
        attributes["operation"],
        attributes["value"],
    );
}
/**
 * @param xmlObject {object}
 * @return {Iterate}
 */
function xmlToIterate(xmlObject) {
    //console.log("xmlToIterate", xmlObject);
    const attributes = xmlObject["$"];
    return new Iterate(
        attributes["key"],
        xmlToExpression(xmlObject["expression"][0]),
    );
}

/**
 * @param xmlObject {object}
 * @return {Expression}
 */
function xmlToExpression(xmlObject) {
    //console.log("xmlToExpression", xmlObject);
    const attributes = xmlObject["$"];
    /** @type {Array} */
    const childExpressions = xmlObject["expression"] ? xmlObject["expression"].map(xmlToExpression) : [];
    /** @type {Array} */
    const childIterates = xmlObject["iterate"] ? xmlObject["iterate"].map(xmlToIterate) : [];
    /** @type {Array} */
    const childItems = xmlObject["item"] ? xmlObject["item"].map(xmlToItem) : [];
    /** @type {Array} */
    const children = childExpressions
        .concat(childIterates)
        .concat(childItems);
    return new Expression(
        ExpressionType.enumValueOf(attributes["type"]),
        children,
    );
}

/**
 * @param xmlObject {object}
 * @return {Rule}
 */
function xmlToRule(xmlObject) {
    //console.log("xmlToRule", xmlObject);
    const attributes = xmlObject["$"];
    return new Rule(
        stringToBoolean[attributes["active"].toLowerCase()],
        attributes["description"],
        xmlToExpression(xmlObject["expression"][0]),
        xmlToChanges(xmlObject["changes"][0]),
    );
}

/**
 * @param xmlObject {object}
 * @return {Rule[]}
 */
function xmlToRules(xmlObject) {
    //console.log("xmlToRules", xmlObject);
    return xmlObject["rule"].map(xmlToRule);
}

/**
 * @param xmlString {string}
 * @param callback {function}
 */
function xmlStringToRules(xmlString, callback) {
    const parser = new xml2js.Parser();
    parser.parseString(xmlString, (err, xmlObject) => {
        //console.log("xmlObject", xmlObject);
        const rules = xmlToRules(xmlObject["rules"]);
        callback(rules);
    });
}

export default xmlStringToRules;
