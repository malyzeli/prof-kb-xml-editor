import js2xmlparser from "js2xmlparser";
import _ from "lodash";
import ArgumentClass from "../model/enum/ArgumentClass";
import ExpressionType from "../model/enum/ExpressionType";
// eslint-disable-next-line no-unused-vars
import {Argument, Change, Expression, Item, Iterate, Rule} from "../model/model";

const options = {
    format: {
        doubleQuotes: true,
    },
};

/**
 * @param argument {Argument}
 */
function argumentToXml(argument) {
    return {
        "@": {
            value: argument.value,
            "class": ArgumentClass.enumValueOf(argument.targetClass).fqdn,
        },
    };
}

/**
 * @param change {Change}
 */
function changeToXml(change) {
    return {
        "@": {
            invoke: change.invoke,
        },

        argument: change.invokeArguments.map((arg) => argumentToXml(arg)),
    };
}

/**
 * @param changes {Change[]}
 */
function changesToXml(changes) {
    return {
        change: changes.map((change) => changeToXml(change)),
    };
}

/**
 * @param item {Item}
 */
function itemToXml(item) {
    return {
        "@": {
            key: item.itemKey,
            operation: item.operation,
            value: item.value,
        },
    };
}

/**
 * @param iterate {Iterate}
 */
function iterateToXml(iterate) {
    return {
        "@": {
            key: iterate.iterateKey,
        },

        expression: expressionToXml(iterate.children[0]),
    };
}

/**
 * @param e {(Expression|Iterate|Item)}
 */
const isExpression = (e) => e.type === ExpressionType.AND || e.type === ExpressionType.OR;

/**
 * @param expression {Expression}
 */
function expressionToXml(expression) {
    return {
        "@": {
            type: expression.type,
        },

        expression: expression.children
            .filter((child) => isExpression(child))
            .map((expression) => expressionToXml(expression)),

        iterate: expression.children
            .filter((child) => child.type === ExpressionType.ITERATE)
            .map((iterate) => iterateToXml(iterate)),

        item: expression.children
            .filter((child) => child.type === ExpressionType.ITEM)
            .map((item) => itemToXml(item)),
    };
}

/**
 * @param rule {Rule}
 */
function ruleToXml(rule) {
    return {
        "@": {
            "description": rule.description,
            "active": rule.active,
        },
        expression: expressionToXml(rule.expression),
        changes: changesToXml(rule.changes),
    };
}

/**
 * @param rules {Rule[]|Rule}
 */
function rulesToXml(rules) {
    return {
        "@": {
            "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
            "xsi:noNamespaceSchemaLocation": "test.xsd",
        },
        rule: _.isArray(rules)
            ? rules.map((rule) => ruleToXml(rule))
            : ruleToXml(rules),
    };
}

/**
 * @param rules {Rule[]}
 * @return {string}
 */
function rulesToXmlString(rules) {
    return js2xmlparser.parse("rules", rulesToXml(rules), options);
}

export default rulesToXmlString;
