import _ from "lodash";
import uuid from "uuid";
import ArgumentClass from "./enum/ArgumentClass";
import ExpressionType from "./enum/ExpressionType";
import ItemOperation from "./enum/ItemOperation";

class BaseEntity {
    constructor() {
        this.uuid = uuid();
    }
}

export class Argument extends BaseEntity {
    /**
     * @param targetClass {object}
     * @param value {(int|string)}
     */
    constructor(targetClass = ArgumentClass[0].name, value = 0) {
        super();
        this.targetClass = targetClass;
        this.value = value;
    }
}

export class Change extends BaseEntity {
    /**
     * @param invoke {string}
     * @param invokeArguments {(Argument[]|Argument)}
     */
    constructor(invoke = "", invokeArguments = []) {
        super();
        this.invoke = invoke;
        this.invokeArguments = _.isArray(invokeArguments) ? invokeArguments : [invokeArguments];
    }
}

class BaseExpression extends BaseEntity {
    /**
     * @param type {string}
     */
    constructor(type) {
        super();
        this.type = type;
    }
}

export class Item extends BaseExpression {
    /**
     * @param itemKey {string}
     * @param operation {string}
     * @param value {string}
     */
    constructor(itemKey = "", operation = ItemOperation[0].name, value = "") {
        super(ExpressionType.ITEM);
        this.itemKey = itemKey;
        this.operation = operation;
        this.value = value;
    }
}

export class Expression extends BaseExpression {
    /**
     * @param type {string}
     * @param children {BaseExpression|BaseExpression[]}
     */
    constructor(type = ExpressionType.AND, children = [new Item()]) {
        super(type);
        this.children = _.isArray(children) ? children : [children];
    }
}

export class Iterate extends BaseExpression {
    /**
     * @param iterateKey {string}
     * @param children {Expression}
     */
    constructor(iterateKey = "", children = new Expression()) {
        super(ExpressionType.ITERATE);
        this.iterateKey = iterateKey;
        this.children = [children];
    }
}

export class Rule extends BaseEntity {
    /**
     * @param active {boolean}
     * @param description {string}
     * @param expression {Expression}
     * @param changes {Change[]|Change}
     */
    constructor(active = true, description = "", expression = new Expression(), changes = [new Change()]) {
        super();
        this.active = active;
        this.description = description;
        this.expression = expression;
        this.changes = _.isArray(changes) ? changes : [changes];
    }
}
