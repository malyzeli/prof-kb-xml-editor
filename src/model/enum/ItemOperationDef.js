/**
 * Item operation value type enum.
 *
 * When adding new type you have to implement it in Item component first!
 *
 * @type {{string}}
 */
export const ItemOperationValueType = {
    /**
     * Renders as
     * <input type="text"/>
     */
    TEXT: "TEXT",
    /**
     * Renders as
     * <input type="number"/>
     */
    NUMBER: "NUMBER",
    /**
     * Renders as
     * <select/>
     */
    SELECT: "SELECT",
};

/**
 * ItemOperation definition class.
 */
export class ItemOperation {
    /**
     * Operation name is exported into resulting XML file and it must be the same as existing operation name defined in target test suite.
     *
     * @type {string}
     */
    name;

    /**
     * Defines type of the input field which is rendered in the editor - see above for available values.
     *
     * @type {string}
     */
    type;

    /**
     * Optional array of values for operations with SELECT type.
     *
     * @type {string[]}
     */
    values;

    /**
     * Optional validate function used for error highlighting.
     * Should return false for invalid value.
     *
     * Default validator accepts any value.
     *
     * @type {function}
     */
    validate = () => true;

    /**
     * @param name {string}
     * @param type {string}
     * @param values {string[]}
     * @param validate {function}
     */
    constructor(name, type, {values = null, validate = null} = {}) {
        this.name = name;
        this.type = type;
        this.values = values;

        /* default enum validator */
        if (type === ItemOperationValueType.SELECT) {
            this.validate = (value) => this.values.includes(value);
        }

        /* use custom validator when provided */
        if (validate) {
            this.validate = validate;
        }
    }
}
