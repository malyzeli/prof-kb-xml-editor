/**
 * Argument definition class.
 *
 * Maps to default Java primitives.
 */
export class ArgumentClass {
    /**
     * Class name can be any unique identifier, it is used only in React application for rendering select box option keys and values.
     *
     * @type {string}
     */
    name;

    /**
     * FQDN (fully qualified domain name) must target valid Java primitive as it is exported directly into resulting XML file.
     *
     * @type {string}
     */
    fqdn;

    /**
     * Optional validate function used for error highlighting.
     * Should return false for invalid value.
     *
     * Default validator accepts any value.
     *
     * @type {function}
     */
    validate = () => true;

    /**
     * @param name {string}
     * @param fqdn {string}
     * @param validate {function}
     */
    constructor(name, fqdn, {validate = null} = {}) {
        this.name = name;
        this.fqdn = fqdn;

        /* use custom validator when provided */
        if (validate) {
            this.validate = validate;
        }
    }
}
