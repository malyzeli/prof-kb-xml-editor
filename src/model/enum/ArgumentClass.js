import ArgumentClass from "../../enums/argumentClasses";
import defineLookup from "./defineLookup";

defineLookup(ArgumentClass, "name", "enumValueOf");
defineLookup(ArgumentClass, "fqdn", "enumValueOfFqdn");

export default ArgumentClass;
