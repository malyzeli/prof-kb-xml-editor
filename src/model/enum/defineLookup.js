/**
 * Defines lookup function used to effectively retrieve value from an array using specified key - think of Java enumValueOf.
 * Internally uses hash table.
 *
 * @param collection {Array} target collection
 * @param lookupKey {string} property key used for retrieval
 * @param targetPropertyName {string} property name where lookup function will be defined
 */
export default function defineLookup(collection, lookupKey, targetPropertyName) {

    const lookupTable = collection.reduce(
        (result, operation) => {
            result[operation[lookupKey]] = operation;
            return result;
        },
        {},
    );

    const lookup = (value) => lookupTable[value];

    Object.defineProperty(collection, targetPropertyName, {value: lookup, enumerable: false});
}
