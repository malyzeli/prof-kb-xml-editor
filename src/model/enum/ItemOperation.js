import ItemOperation from "../../enums/itemOperations";
import defineLookup from "./defineLookup";

defineLookup(ItemOperation, "name", "enumValueOf");

export default ItemOperation;
