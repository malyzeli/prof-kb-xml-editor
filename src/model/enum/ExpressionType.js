/**
 * Expression type enum.
 * Contains all node types in expression tree, including item and iterate.
 *
 * @type {{string}}
 */
const ExpressionType = {
    AND: "and",
    OR: "or",
    ITEM: "item",
    ITERATE: "iterate",
};

{
    /** @param string {string} */
    const enumValueOf = (string) => ExpressionType[string.toUpperCase()];

    Object.defineProperty(ExpressionType, "enumValueOf", {value: enumValueOf, enumerable: false});
}

export default ExpressionType;
