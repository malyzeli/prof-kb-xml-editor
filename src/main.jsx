import React from "react";
import ReactDOM from "react-dom";
import {I18nextProvider} from "react-i18next";
import Application from "./Application";
import i18n from "./i18n";

const container = document.getElementById("application-root");

const application = (
    <I18nextProvider i18n={i18n}>
        <Application/>
    </I18nextProvider>
);

// eslint-disable-next-line react/no-render-return-value
const rootInstance = ReactDOM.render(application, container);

//noinspection JSUnresolvedVariable
if (module.hot) {
    //noinspection JSUnusedGlobalSymbols,JSUnresolvedFunction,JSUnresolvedVariable
    require("react-hot-loader/Injection")
        .RootInstanceProvider
        .injectProvider({
            getRootInstances: () => [rootInstance],
        });
}
