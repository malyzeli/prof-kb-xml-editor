import classNames from "classnames";
import FileSaver from "file-saver";
import update from "immutability-helper";
import PropTypes from "prop-types";
import React from "react";
import {translate} from "react-i18next";
import SyntaxHighlighter from "react-syntax-highlighter";
import {darcula} from "react-syntax-highlighter/dist/styles";
import sanitize from "sanitize-filename";
import Rule from "./components/Rule";
import i18n from "./i18n";
import * as Model from "./model/model";
import "./styles/index.less";
import rulesToXmlString from "./xml/rulesToXmlString";
import xmlStringToRules from "./xml/xmlStringToRules";

/**
 * @param lang {string|null}
 * @return {string}
 */
const NEXT_LANG = (lang = null) => {
    if (!lang) {
        return "en";
    }
    if (lang === "en") {
        return "cs";
    }
    if (lang === "cs") {
        return "en";
    }
};

/** @param rule {Rule} */
const EXPORT_SINGLE_FILENAME = (rule) => `${rule.description} ${new Date().toISOString()}.xml`;
const EXPORT_ALL_FILENAME = () => `Rules ${new Date().toISOString()}.xml`;

const XML_MIME_TYPE = {type: "text/xml;charset=utf-8"};
const LOAD_FILE_ACCEPT = "application/xml, text/xml";

const REGEX_HELP_URL = "https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html";
const openRegexHelp = () => window.open(REGEX_HELP_URL, "_blank");

/**
 * @param fileName {string}
 * @param xmlContent {string}
 */
function saveXmlFile(fileName, xmlContent) {
    const blob = new Blob([xmlContent], XML_MIME_TYPE);
    FileSaver.saveAs(blob, sanitize(fileName));
    alert(i18n.t("message.file-saved", {fileName}));
}

/**
 * Helper function for deep updates.
 *
 * @param nodeAddress {number[]}
 * @param command {object}
 */
function updateNode(nodeAddress, command) {
    /**
     * @param subAddress {number[]}
     */
    const level = (subAddress) => {
        if (subAddress.length < 1) {
            return command;
        }
        return {
            children: {
                [subAddress[0]]: level(subAddress.slice(1)),
            },
        };
    };

    return {
        // root level
        expression: level(nodeAddress.slice(1)),
    };
}

class Application extends React.PureComponent {
    //noinspection JSUnusedGlobalSymbols
    static propTypes = {
        t: PropTypes.func,
    };

    state = {
        rules: [new Model.Rule(true, i18n.t("default.rule-description"))],
        selectedRuleIndex: 0,
        output: false,
        lang: NEXT_LANG(),
    };

    //<editor-fold desc="import/export">

    handleLoadFromFile = (event) => {
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = (upload) => {
            xmlStringToRules(
                upload.target.result, // file content
                (rules) => this.setState({
                    rules, // parsed rules
                    selectedRuleIndex: 0,
                }),
            );
            alert(i18n.t("message.file-loaded", {fileName: file.name}));
        };
        reader.readAsText(file);
    };

    handleSaveSingle = () => {
        const {rules, selectedRuleIndex} = this.state;
        const rule = rules[selectedRuleIndex];
        saveXmlFile(
            EXPORT_SINGLE_FILENAME(rule),
            rulesToXmlString(rule),
        );
    };

    handleSaveAll = () => {
        const {rules} = this.state;
        saveXmlFile(
            EXPORT_ALL_FILENAME(),
            rulesToXmlString(rules),
        );
    };

    //</editor-fold>

    //<editor-fold desc="state updates">

    handleToggleOutput = () => this.setState({output: !this.state.output});

    handleChangeLang = () => {
        const lang = NEXT_LANG(this.state.lang);
        i18n.changeLanguage(lang);
        this.setState({lang});
    };

    handleSelectRule = (ruleIndex) => this.setState({selectedRuleIndex: ruleIndex});

    handleNewRuleSet = () => {
        if (confirm(i18n.t("message.confirm-new-rule-set"))) {
            this.setState({
                rules: [new Model.Rule(true, i18n.t("default.rule-description"))],
                selectedRuleIndex: 0,
            });
        }
    };

    handleAddRule = () => {
        const {rules} = this.state;
        this.setState({
            rules: update(rules, {
                $push: [new Model.Rule(true, i18n.t("editor.new-rule-description", {id: rules.length + 1}))],
            }),
            selectedRuleIndex: rules.length,
        });
    };

    handleMoveUp = (ruleIndex) => {
        if (ruleIndex === 0) {
            return;
        }
        const {rules} = this.state;
        this.setState({
            rules: update(rules, {
                [ruleIndex]: {
                    $set: rules[ruleIndex - 1],
                },
                [ruleIndex - 1]: {
                    $set: rules[ruleIndex],
                },
            }),
            selectedRuleIndex: ruleIndex - 1,
        });
    };

    handleMoveDown = (ruleIndex) => {
        const {rules} = this.state;
        if (ruleIndex >= rules.length - 1) {
            return;
        }
        this.setState({
            rules: update(rules, {
                [ruleIndex]: {
                    $set: rules[ruleIndex + 1],
                },
                [ruleIndex + 1]: {
                    $set: rules[ruleIndex],
                },
            }),
            selectedRuleIndex: ruleIndex + 1,
        });
    };

    //<editor-fold desc="rule state">

    handleRemoveRule = (ruleIndex) => {
        this.setState({
            rules: update(this.state.rules, {
                $splice: [[ruleIndex, 1]],
            }),
        });
    };

    handleRuleChangeActive = (ruleIndex) => {
        const rule = this.state.rules[ruleIndex];
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: {active: {$set: !rule.active}},
            }),
        });
    };

    handleRuleChangeDescription = (ruleIndex, description) => {
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: {description: {$set: description}},
            }),
        });
    };

    handleRuleAddChange = (ruleIndex) => {
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: {changes: {$push: [new Model.Change()]}},
            }),
        });
    };

    //</editor-fold>

    //<editor-fold desc="expression state">

    handleChangeExpressionType = (ruleIndex, nodeAddress, type) => {
        // console.log("onChangeExpressionType", `r(${ruleIndex})`, `n(${nodeAddress})`, type);
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: updateNode(nodeAddress, {type: {$set: type}}),
            }),
        });
    };

    handleAddItem = (ruleIndex, nodeAddress) => {
        // console.log("onAddItem", `r(${ruleIndex})`, `n(${nodeAddress})`);
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: updateNode(nodeAddress, {children: {$push: [new Model.Item()]}}),
            }),
        });
    };

    handleAddExpression = (ruleIndex, nodeAddress) => {
        // console.log("onAddExpression", `r(${ruleIndex})`, `n(${nodeAddress})`);
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: updateNode(nodeAddress, {children: {$push: [new Model.Expression()]}}),
            }),
        });
    };

    handleAddIterate = (ruleIndex, nodeAddress) => {
        // console.log("onAddIterate", `r(${ruleIndex})`, `n(${nodeAddress})`);
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: updateNode(nodeAddress, {children: {$push: [new Model.Iterate()]}}),
            }),
        });
    };

    handleRemoveExpressionNode = (ruleIndex, nodeAddress) => {
        /** @type {Number[]} */
        const parentNodeAddress = nodeAddress.slice(0, -1);
        /** @type {Number[]} */
        const relativeNodeAddress = nodeAddress.slice(-1); // relative to parent node
        /** @type {Number[]} */
        const spliceParams = relativeNodeAddress.concat(1); // results in splice(address, 1) call

        // todo: remove empty parent iterate or disable removing iterate child expression

        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: updateNode(parentNodeAddress, {children: {$splice: [spliceParams]}}),
            }),
        });
    };

    handleRemoveExpression = (ruleIndex, nodeAddress) => {
        // console.log("onRemoveExpression", `r(${ruleIndex})`, `n(${nodeAddress})`);
        if (nodeAddress.length < 2) {
            return;
        } // prevent removing root expression
        this.handleRemoveExpressionNode(ruleIndex, nodeAddress);
    };

    //</editor-fold>

    //<editor-fold desc="item state">

    handleChangeItemKey = (ruleIndex, nodeAddress, key) => {
        // console.log("onChangeItemKey", `r(${ruleIndex})`, `n(${nodeAddress})`, key);
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: updateNode(nodeAddress, {itemKey: {$set: key}}),
            }),
        });
    };

    handleChangeItemOperation = (ruleIndex, nodeAddress, operation) => {
        // console.log("onChangeItemOperation", `r(${ruleIndex})`, `n(${nodeAddress})`, operation);
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: updateNode(nodeAddress, {operation: {$set: operation}}),
            }),
        });
    };

    handleChangeItemValue = (ruleIndex, nodeAddress, value) => {
        // console.log("onChangeItemValue", `r(${ruleIndex})`, `n(${nodeAddress})`, value);
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: updateNode(nodeAddress, {value: {$set: value}}),
            }),
        });
    };

    handleRemoveItem = (ruleIndex, nodeAddress) => {
        // console.log("onRemoveItem", `r(${ruleIndex})`, `n(${nodeAddress})`);
        this.handleRemoveExpressionNode(ruleIndex, nodeAddress);
    };

    //</editor-fold>

    //<editor-fold desc="iterate state">

    handleChangeIterateKey = (ruleIndex, nodeAddress, key) => {
        // console.log("onChangeIterateKey", `r(${ruleIndex})`, `n(${nodeAddress})`, key);
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: updateNode(nodeAddress, {iterateKey: {$set: key}}),
            }),
        });
    };

    handleRemoveIterate = (ruleIndex, nodeAddress) => {
        // console.log("onRemoveIterate", `r(${ruleIndex})`, `n(${nodeAddress})`);
        this.handleRemoveExpressionNode(ruleIndex, nodeAddress);
    };

    //</editor-fold>

    //<editor-fold desc="change state">

    handleRemoveChange = (ruleIndex, changeIndex) => {
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: {
                    changes: {$splice: [[changeIndex, 1]]},
                },
            }),
        });
    };

    handleChangeChangeInvoke = (ruleIndex, changeIndex, invoke) => {
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: {
                    changes: {
                        [changeIndex]: {invoke: {$set: invoke}},
                    },
                },
            }),
        });
    };

    handleChangeAddArgument = (ruleIndex, changeIndex) => {
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: {
                    changes: {
                        [changeIndex]: {invokeArguments: {$push: [new Model.Argument()]}},
                    },
                },
            }),
        });
    };

    //</editor-fold>

    //<editor-fold desc="argument state">

    handleRemoveArgument = (ruleIndex, changeIndex, argumentIndex) => {
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: {
                    changes: {
                        [changeIndex]: {invokeArguments: {$splice: [[argumentIndex, 1]]}},
                    },
                },
            }),
        });
    };

    handleArgumentChangeClass = (ruleIndex, changeIndex, argumentIndex, targetClass) => {
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: {
                    changes: {
                        [changeIndex]: {
                            invokeArguments: {
                                [argumentIndex]: {targetClass: {$set: targetClass}},
                            },
                        },
                    },
                },
            }),
        });
    };

    handleArgumentChangeValue = (ruleIndex, changeIndex, argumentIndex, value) => {
        this.setState({
            rules: update(this.state.rules, {
                [ruleIndex]: {
                    changes: {
                        [changeIndex]: {
                            invokeArguments: {
                                [argumentIndex]: {value: {$set: value}},
                            },
                        },
                    },
                },
            }),
        });
    };

    //</editor-fold>

    //</editor-fold>

    //noinspection JSMethodCanBeStatic
    componentWillMount() {
        i18n.changeLanguage(this.state.lang);
        window.addEventListener("beforeunload", (e) => {
            const confirmationMessage = i18n.t("message.confirm-leave-page");

            e.returnValue = confirmationMessage; // Gecko, Trident, Chrome 34+
            return confirmationMessage; // Gecko, WebKit, Chrome <34
        });
    }

    render() {
        const {t} = this.props;
        const {rules, selectedRuleIndex, output, lang} = this.state;

        const className = classNames(
            "application",
            output && "output",
        );

        const selectedRule = rules[selectedRuleIndex];

        return (
            <div className={className}>

                <section className="menu">
                    <h1>
                        {
                            t("menu.rule-editor")
                        }
                    </h1>

                    <div className="buttons">
                        <label title={t("menu.add-rule")}>
                            <button onClick={this.handleAddRule}>
                                {
                                    t("menu.add-rule")
                                }
                            </button>
                        </label>

                        <label title={t("menu.new-rule-set")}>
                            <button onClick={this.handleNewRuleSet}>
                                {
                                    t("menu.new-rule-set")
                                }
                            </button>
                        </label>

                        <label className="load"
                               title={t("menu.load-from-file")}
                        >
                            {
                                t("menu.load-from-file")
                            }
                            <input type="file"
                                   accept={LOAD_FILE_ACCEPT}
                                   onChange={this.handleLoadFromFile}
                            />
                        </label>

                        <label title={t("menu.save-single")}>
                            <button onClick={this.handleSaveSingle}>
                                {
                                    t("menu.save-single")
                                }
                            </button>
                        </label>

                        <label title={t("menu.save-all")}>
                            <button onClick={this.handleSaveAll}>
                                {
                                    t("menu.save-all")
                                }
                            </button>
                        </label>

                        <label title={t("menu.help-regex")}>
                            <button onClick={openRegexHelp}>
                                {
                                    t("menu.help-regex")
                                }
                            </button>
                        </label>

                        <label title={t("menu.toggle-output")}>
                            <button onClick={this.handleToggleOutput}>
                                {
                                    output ?
                                    t("menu.show-editor") :
                                    t("menu.show-output")
                                }
                            </button>
                        </label>

                        <label title={t("menu.switch-language")}>
                            <button onClick={this.handleChangeLang}>
                                {
                                    NEXT_LANG(lang)
                                        .toUpperCase()
                                }
                            </button>
                        </label>
                    </div>
                </section>

                <section className="content">
                    <nav className="rules">
                        <ul>
                            {
                                rules.map((rule, index) => (
                                    <li key={index}
                                        className={classNames({"selected": index === selectedRuleIndex})}
                                    >
                                        <button onClick={() => this.handleSelectRule(index)}>
                                            {
                                                rule.description
                                            }
                                        </button>
                                    </li>
                                ))
                            }
                        </ul>
                    </nav>

                    <section className="main">
                        {
                            !output && selectedRule &&
                            <section className="editor">
                                <ul className="rule-list">
                                    <Rule key={selectedRule.uuid}
                                          active={selectedRule.active}
                                          description={selectedRule.description}
                                          expression={selectedRule.expression}
                                          changes={selectedRule.changes}
                                          onMoveUp={() => this.handleMoveUp(selectedRuleIndex)}
                                          onMoveDown={() => this.handleMoveDown(selectedRuleIndex)}
                                          onChangeActive={() => this.handleRuleChangeActive(selectedRuleIndex)}
                                          onChangeDescription={(description) => this.handleRuleChangeDescription(
                                              selectedRuleIndex,
                                              description,
                                          )}
                                          onAddChange={(nodeAddress) => this.handleRuleAddChange(selectedRuleIndex, nodeAddress)}
                                          onRemove={(nodeAddress) => this.handleRemoveRule(selectedRuleIndex, nodeAddress)}
                                          onChangeExpressionType={(nodeAddress, type) => this.handleChangeExpressionType(
                                              selectedRuleIndex,
                                              nodeAddress,
                                              type,
                                          )}
                                          onAddItem={(nodeAddress) => this.handleAddItem(selectedRuleIndex, nodeAddress)}
                                          onAddExpression={(nodeAddress) => this.handleAddExpression(selectedRuleIndex, nodeAddress)}
                                          onAddIterate={(nodeAddress) => this.handleAddIterate(selectedRuleIndex, nodeAddress)}
                                          onRemoveExpression={(nodeAddress) => this.handleRemoveExpression(selectedRuleIndex, nodeAddress)}
                                          onChangeItemKey={(nodeAddress, key) => this.handleChangeItemKey(
                                              selectedRuleIndex,
                                              nodeAddress,
                                              key,
                                          )}
                                          onChangeItemOperation={(nodeAddress, operation) => this.handleChangeItemOperation(
                                              selectedRuleIndex,
                                              nodeAddress,
                                              operation,
                                          )}
                                          onChangeItemValue={(nodeAddress, value) => this.handleChangeItemValue(
                                              selectedRuleIndex,
                                              nodeAddress,
                                              value,
                                          )}
                                          onRemoveItem={(nodeAddress) => this.handleRemoveItem(selectedRuleIndex, nodeAddress)}
                                          onChangeIterateKey={(nodeAddress, key) => this.handleChangeIterateKey(
                                              selectedRuleIndex,
                                              nodeAddress,
                                              key,
                                          )}
                                          onRemoveIterate={(nodeAddress) => this.handleRemoveIterate(selectedRuleIndex, nodeAddress)}
                                          onChangeChangeInvoke={(changeIndex, invoke) => this.handleChangeChangeInvoke(
                                              selectedRuleIndex,
                                              changeIndex,
                                              invoke,
                                          )}
                                          onChangeAddArgument={(changeIndex) => this.handleChangeAddArgument(
                                              selectedRuleIndex,
                                              changeIndex,
                                          )}
                                          onRemoveChange={(changeIndex) => this.handleRemoveChange(selectedRuleIndex, changeIndex)}
                                          onArgumentChangeClass={(changeIndex, argumentIndex,
                                                                  targetClass) => this.handleArgumentChangeClass(
                                              selectedRuleIndex,
                                              changeIndex,
                                              argumentIndex,
                                              targetClass,
                                          )}
                                          onArgumentChangeValue={(changeIndex, argumentIndex, value) => this.handleArgumentChangeValue(
                                              selectedRuleIndex,
                                              changeIndex,
                                              argumentIndex,
                                              value,
                                          )}
                                          onRemoveArgument={(changeIndex, argumentIndex) => this.handleRemoveArgument(
                                              selectedRuleIndex,
                                              changeIndex,
                                              argumentIndex,
                                          )}
                                    />
                                </ul>
                            </section>
                        }

                        {
                            output &&
                            <section className="output">
                                <SyntaxHighlighter language="xml"
                                                   style={darcula}
                                >
                                    {
                                        rulesToXmlString(selectedRule)
                                    }
                                </SyntaxHighlighter>
                            </section>
                        }
                    </section>
                </section>
            </div>
        );
    }
}

export default translate()(Application);
