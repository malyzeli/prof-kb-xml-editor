# Tasks

## Core Functionality

* [x] create and edit test rules
* [x] import XML file with existing rules
* [x] export individual rules to XML
* [x] export all rules into single XML
* [x] show XML preview output directly in editor
* [x] warn before page exit
* [x] switch between EN/CS languages

## Important Issues

* [ ] error handling when loading invalid XML file
* [ ] check all rules for errors
* [ ] disable XML export when invalid rules present

## Optional Features

* [ ] duplicate existing rule
* [ ] sorting rules by name
* [ ] sorting rules by drag & drop
* [ ] search redundant rules
* [ ] editor folding for better visibility
